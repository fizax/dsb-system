/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.dsb.web;

import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.celestial.dsb.core.MetatagController;
import com.celestial.dsb.core.URLController;
import com.celestial.dsb.utils.SimpleJsonMessage;

import domain.Bookmark;

/**
 *
 * @author Selvyn
 */
@Path("/services")
public class HancServicePort implements IHancServicePort
{
	
    @Override
    @GET
    @Path("/sayhello")
    public Response sayHtmlHelloTest()
    {
        String result = "<html> " + "<title>" + "hanc" + "</title>"
                + "<body><h1>" + "dsb is running..." + "</h1></body>" + "</html> ";

        return Response.status(200).entity(result).build();
    }
    
    @Override
    @GET
    @Path("/getAllTags")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAllTags()
    {
    	MetatagController mc = new MetatagController();
    	ArrayList<String> res = mc.getAllTags();
    	return Response.ok(res, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @Override
    @GET
    @Path("/getAllURL")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAllURL()
    {
    	URLController urlc = new URLController();
    	ArrayList<Bookmark> res = urlc.getAllLinks();
    	return Response.ok(res, MediaType.APPLICATION_JSON_TYPE).build();
        // Use a controller to get all the links, return the result
        //++ insert code here
    }

    @Override
    @GET
    @Path("/get/{tags}")
    public Response getSavedURLWithInfo(@PathParam("tags")String tags)
    {
    	URLController suc = new URLController();
    	ArrayList<Bookmark> res = suc.getUrlForTag(tags);
    	return Response.ok(res, MediaType.APPLICATION_JSON_TYPE).build();
    }

    @Override
    @GET
    @Path("/save/{url}/{description}/{tags}")
    public Response saveNewURLWithInfo( @PathParam("url")String url,
                                        @PathParam("description")String description,
                                        @PathParam("tags")String tags )
    {
        URLController suc = new URLController();
        suc.saveURL(url, description, tags);
        return Response.status(200).entity( new SimpleJsonMessage( url + "//" + description + "//" + tags ) ).build();
    }

    @Override
    @POST
    @Path("/save")
    public Response saveNewURLWithInfoFromForm( @FormParam("url")String url,
                                        @FormParam("description")String description,
                                        @FormParam("tags")String tags )
    {
        URLController suc = new URLController();
        suc.saveURL(url, description, tags);
        return Response.status(200).entity( new SimpleJsonMessage( url + "//" + description + "//" + tags ) ).build();
    }

}
